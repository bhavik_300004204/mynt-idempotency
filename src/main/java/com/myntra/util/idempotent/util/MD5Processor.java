package com.myntra.util.idempotent.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class MD5Processor which will be used to generate MD5 key for the
 * serialization.
 *
 * @author Bhavikkumar Ambani
 */
public class MD5Processor {

	private static final Logger LOGGER = LoggerFactory.getLogger(MD5Processor.class);

    /**
     * The Constant DEFAULT_HASHING.
     */
    private static final String DEFAULT_HASHING = "SHA1";

    /**
     * Gets the md5.
     *
     * @param input the input
     * @return the md5
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static String getMD5(Object input) throws IOException {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        try {
            ObjectOutputStream o = new ObjectOutputStream(b);
            try {
                o.writeObject(input);
            } catch (Exception e) {
				LOGGER.error("Exception while generating MD5 value for key # " + input, e);
            } finally {
                try {
                    o.flush();
                    o.close();
				} catch (Exception e) {
					LOGGER.error("Exception while closing stream for key # " + input, e);
				}
            }
            MessageDigest md = MessageDigest.getInstance(DEFAULT_HASHING);
            byte[] messageDigest = md.digest(b.toByteArray());
            BigInteger number = new BigInteger(1, messageDigest);
            StringBuilder hashText = new StringBuilder(number.toString(16));
			// Now we need to zero pad it to generate full 32 chars.
			while (hashText.length() < 32) {
                hashText.insert(0, "0");
            }
			LOGGER.debug("MD5 Generator # key [ " + input + " ], MD5 value[ " + hashText + " ]");
            return hashText.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                b.flush();
                b.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * The main method.
     *
     * @param args the arguments
     * @throws NoSuchAlgorithmException the no such algorithm exception
     * @throws IOException              Signals that an I/O exception has occurred.
     */
    public static void main(String[] args) throws NoSuchAlgorithmException, IOException {
        long time = System.currentTimeMillis();
        LOGGER.info(getMD5("BhavikAmbani"));
        LOGGER.info("Total time taken ### " + (System.currentTimeMillis() - time));
    }
}
