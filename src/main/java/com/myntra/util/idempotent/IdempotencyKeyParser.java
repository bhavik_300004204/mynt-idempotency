package com.myntra.util.idempotent;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.support.AopUtils;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.core.ParameterNameDiscoverer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myntra.util.idempotent.types.DeDupParamType;
import com.myntra.util.idempotent.types.KeySerializationType;
import com.myntra.util.idempotent.util.MD5Processor;

/**
 * The Class IdempotencyKeyParser.
 *
 * @author Bhavikkumar Ambani
 */
class IdempotencyKeyParser {
	
	/** Default prefix key. */
    private static final String DEFAULT_KEY_PREFIX = "DD";

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(IdempotencyKeyParser.class);

    /** The key which will be added before any lock key. */
    private final String KEY_PREFIX;

    /**
     * The JSON request parameter mapper.
     */
    private final ObjectMapper mapper;

    /**
     * The param name discoverer.
     */
    private final ParameterNameDiscoverer PARAM_NAME_DISCOVERER;

    /**
     * Instantiates a new idempotency key parser.
     */
    public IdempotencyKeyParser() {
        this(DEFAULT_KEY_PREFIX);
    }

    /**
	 * Instantiates a new idempotency key parser.
	 *
	 * @param keyPrefix
	 *            the key prefix
	 */
    public IdempotencyKeyParser(final String keyPrefix) {
        if (keyPrefix == null || "".equals(keyPrefix) || keyPrefix.trim().length() ==0)
            this.KEY_PREFIX = "DNS";
        else
            this.KEY_PREFIX = keyPrefix;
        mapper = new ObjectMapper();
        PARAM_NAME_DISCOVERER = new DefaultParameterNameDiscoverer();
    }

    /**
     * Gets the idempotency key from the request parameter and key type
     * <code>DeDupParamType</code> provided.
     *
     * @param jointPoint the joint point
     * @param annotation the annotation {@link DeDup}, which will contain properties
     *                   such as {@link DeDup#paramType()} etc.
     *
     * @return the idempotency key which will be used to identify request uniquely
     *
     * @throws NoSuchFieldException     the no such field exception
     * @throws SecurityException        the security exception
     * @throws IllegalArgumentException the illegal argument exception
     * @throws IllegalAccessException   the illegal access exception
     * @see DeDupParamType
     */
    public String getIdempotencyKey(JoinPoint jointPoint, DeDup annotation)
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        return getIdempotencyKey(jointPoint, annotation, annotation.paramType());
    }

    /**
     * Gets the idempotency key.
     *
     * @param jointPoint           the joint point
     * @param annotation           the annotation
     * @param deDupParamType the idempotency param type
     *
     * @return the idempotency key
     *
     * @throws NoSuchFieldException     the no such field exception
     * @throws SecurityException        the security exception
     * @throws IllegalArgumentException the illegal argument exception
     * @throws IllegalAccessException   the illegal access exception
     */
    private String getIdempotencyKey(JoinPoint jointPoint, DeDup annotation,
                                     DeDupParamType deDupParamType)
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        String key = null;
        switch (deDupParamType) {
            case REQUEST_PARAM:
                key = getStrParamKey(jointPoint, annotation);
                break;
            case REQUEST_PARAM_PROPERTY:
                key = getObjectParamKey(jointPoint, annotation);
                break;
            case MULTI_KEY_PARAM:
                try {
                    key = getJSONParamKey(jointPoint, annotation);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            default:
	            	LOGGER.info("Default for Idempotency key type.");
	            	break;
        }
        key = DEFAULT_KEY_PREFIX + "#" + KEY_PREFIX + "#" + key;
        LOGGER.info("Generated Key for Locking/Unlocking # " + key);
        return key;
    }

    /**
	 * Gets the JSON param key.
	 *
	 * @param jointPoint
	 *            the joint point
	 * @param annotation
	 *            the annotation
	 * @return the JSON param key
	 * @throws NoSuchFieldException
	 *             the no such field exception in case of problem in parsing JSON
	 *             input
	 * @throws SecurityException
	 *             the security exception in case of Field is not accessible.
	 * @throws IllegalAccessException
	 *             the illegal access exception in case of Field is not accessible
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
    private String getJSONParamKey(JoinPoint jointPoint, DeDup annotation) throws NoSuchFieldException,
            SecurityException, IllegalAccessException, java.io.IOException {
        StringBuilder sb = new StringBuilder(512);
        String keyObject = annotation.keyObject();
        @SuppressWarnings("unchecked") // Suppressed for Map.class to infer generic type
                Map<String, List<String>> readValue = mapper.readValue(keyObject, Map.class);
        Set<String> keySet = readValue.keySet();
        for (String keyObj : keySet) {
            Object requestValues = getKeyObject(keyObj, jointPoint.getArgs(), ((MethodSignature) jointPoint.getSignature()).getMethod(), jointPoint.getTarget().getClass());
            List<String> paramList = readValue.get(keyObj);
			if (paramList == null || paramList.isEmpty()) {
					sb.append(keyObj).append("#");
			}
			for (String param : paramList) {
				Field f = requestValues.getClass().getDeclaredField(param);
				f.setAccessible(true);
				Object fieldValue = f.get(requestValues);
				String val = generateKeyBySerializer(fieldValue, annotation);
				sb.append(val).append("#");
			}
        }
        if (sb.length() > 0)
            sb.setLength(sb.length() - 1); // Removes unnecessary last # from the string
        return sb.toString();
    }

    /**
     * Gets the key object for the idempotency.
     *
     * @param keyExpr            the key expr
     * @param parameterArguments the args
     * @param method             the method
     * @param targetClass        the target class
     *
     * @return the key object
     */
    private Object getKeyObject(String keyExpr, Object[] parameterArguments, Method method, Class<?> targetClass) {
        Method targetMethod = AopUtils.getMostSpecificMethod(method, targetClass);
        String[] params = PARAM_NAME_DISCOVERER.getParameterNames(targetMethod);
        for (int i = 0; i < params.length; i++) {
            if (params[ i ].equals(keyExpr))
                return parameterArguments[ i ];
        }
        return null;
    }

    /**
	 * Gets the object param key.
	 *
	 * @param jointPoint
	 *            the joint point
	 * @param annotation
	 *            the annotation
	 * @return the object param key
	 * @throws NoSuchFieldException
	 *             the no such field exception in case of problem in parsing JSON
	 *             input
	 * @throws SecurityException
	 *             the security exception in case of Field is not accessible.
	 * @throws IllegalArgumentException
	 *             the illegal argument exception
	 * @throws IllegalAccessException
	 *             the illegal access exception in case of Field is not accessible
	 */
    private String getObjectParamKey(JoinPoint jointPoint, DeDup annotation)
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        Object requestValues = getKeyObject(annotation.keyObject(), jointPoint.getArgs(), ((MethodSignature) jointPoint.getSignature()).getMethod(), jointPoint.getTarget().getClass());
        Field f = requestValues.getClass().getDeclaredField(annotation.classPropertyName());
        f.setAccessible(true);
		return generateKeyBySerializer(f.get(requestValues), annotation);
    }

    /**
     * Gets the str param key.
     *
     * @param jointPoint the joint point
     * @param annotation the annotation
     *
     * @return the str param key
     *
     * @throws SecurityException the security exception
     */
    private String getStrParamKey(JoinPoint jointPoint, DeDup annotation)
            throws SecurityException {
        Object requestValues = getKeyObject(annotation.keyObject(), jointPoint.getArgs(), ((MethodSignature) jointPoint.getSignature()).getMethod(), jointPoint.getTarget().getClass());
		return requestValues != null ? generateKeyBySerializer(requestValues, annotation) : null;
    }
    
	/**
	 * Generate key by serializer provided in the configuration of the
	 * annotation.Default is {@link KeySerializationType.TO_STRING}
	 *
	 * @param fieldValue
	 *            the field value of the request/method parameter
	 * @param annotation
	 *            the annotation
	 * @return the string containing encoding
	 */
	private String generateKeyBySerializer(Object fieldValue, DeDup annotation) {
		String result;
		switch (annotation.keySerializationType()) {
		case MD5:
			try {
				result = MD5Processor.getMD5(fieldValue != null ? fieldValue.toString() : "");
			} catch (IOException e) {
				result = String.valueOf(fieldValue.hashCode());
				LOGGER.error("Exception while getting MD5 value of # " + fieldValue, e);
			}
			break;
		case HASHCODE:
			result = String.valueOf(fieldValue.hashCode());
			break;
		case TO_STRING:
		default:
			result = fieldValue.toString();
			break;
		}
		return result;
	}

}
