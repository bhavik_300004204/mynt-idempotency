package com.myntra.util.idempotent.lock.manager.redis;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;

import org.redisson.Redisson;
import org.redisson.RedissonRedLock;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.myntra.util.idempotent.lock.manager.BulkProcessLockManager;

/**
 * The Class REDISBulkProcessLockManager.
 *
 * @author Bhavikkumar Ambani
 */
public final class REDISBulkProcessLockManager implements BulkProcessLockManager {

	private static final Logger LOGGER = LoggerFactory.getLogger(REDISBulkProcessLockManager.class);

    /**
     * Hosts should be configured in the spring configuration, if it is single NODE
     * REDIS cache, then there will be only one node.
     */
    private final String redisHosts;
    /**
     * The is clustered.
     */
    private final boolean isClustered;
    /**
     * Type of the REDIS clustered, it can be NON_CLUSTERED or CLUSTERED.
     */
    private RedissonClient redissonClient;
    /**
     * The is REDIS connected.
     */
    private boolean isREDISConnected = false;

    /**
     * The default port in case of REDIS port is not provided in configuration parameter.
     */
    private static final Integer DEFAULT_REDIS_PORT = 6379;

    /**
     * The node count.
     */
    private int nodeCount;

    /**
     * Instantiates a new REDIS bulk process lock manager.
     *
     * @param redisHosts          the redis hosts
     * @param strRedisClusterType the str redis cluster type
     */
    public REDISBulkProcessLockManager(final String redisHosts, final String strRedisClusterType) {
        super();
        this.redisHosts = redisHosts;
        /*
      The redis cluster type.
     */
        REDISCLUSTERTYPE redisClusterType = REDISCLUSTERTYPE.valueOf(strRedisClusterType);
        switch (redisClusterType) {
            case CLUSTERED:
                isClustered = true;
                break;
            case NON_CLUSTERED:
                isClustered = false;
                break;
            default:
                isClustered = false;
                break;
        }
        try {
            redissonClient = createClient();
            isREDISConnected = true;
        } catch (Exception e) {
			LOGGER.error("Exception while creating REDIS Connection.", e);
            redissonClient = null;
        }
    }

    /**
     * Parses the redis address.
     *
     * @param address     the address
     * @param defaultPort the default port
     * @return the list
     */
    private List<String> parseRedisAddress(String address, int defaultPort) {
        List<String> list = new ArrayList<String>();
        String[] array = address.split(",");
        for (String s : array) {
            nodeCount++;
            if (!s.startsWith("redis://")) {
                s = "redis://" + s;
                LOGGER.info(s);
            }
            list.add(s);
        }
		LOGGER.info("REDIS Hosts List for DeDuplication Manager ### " + list);
        return list;
    }

    /**
     * Creates the client.
     *
     * @return the redisson client
     */
    private RedissonClient createClient() {
        final Config config = new Config();
        List<String> addressList = parseRedisAddress(redisHosts, DEFAULT_REDIS_PORT);
        if (isClustered) {
            config.useClusterServers().setScanInterval(60).addNodeAddress(addressList.toArray(new String[addressList.size()])).setRetryInterval(100);
        } else {
            config.useSingleServer().setAddress(addressList.get(0)).setRetryInterval(100);
        }
        return Redisson.create(config);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.myntra.util.idempotent.lock.manager.BulkProcessLockManager#lock(java.lang
     * .String)
     */
    @Override
    public boolean lock(final String key) throws Exception {
        long currTime = System.currentTimeMillis();
        boolean lockAcquired = false;
        if (isREDISConnected) {
			LOGGER.info("Acquiring Lock on # " + key);
            if (key == null || key.trim().length() == 0)
                throw new NullPointerException("Key for lock bulk process is passed as null.");
            Lock lock = getLockInstance(key);
            boolean tryLock = lock.tryLock();
            if (tryLock) {
                lock.lock();
                lockAcquired = true;
            }
			LOGGER.info("Time consumed in acquiring lock on # " + key + " is # " + (System.currentTimeMillis() - currTime) + " [ Milli Seconds ]");
        } else {
			LOGGER.info("REDIS is not available, hence not acquiring lock on  Key#  [ " + key + " ].");
            throw new Exception("REDIS is not available.");
        }
        return lockAcquired;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.myntra.util.idempotent.lock.manager.BulkProcessLockManager#unlock(java.
     * lang.String)
     */
    @Override
    public boolean unlock(final String key) throws Exception {
        long currTime = System.currentTimeMillis();
        LOGGER.info("Releasing Lock on # " + key);
        if (key == null || key.trim().length() == 0)
            throw new NullPointerException("Key for unlock bulk process is passed as null.");
        Lock lock = getLockInstance(key);
        lock.unlock();
        LOGGER.info("Time consumed in releasing lock on # " + key + " is # "
                + (System.currentTimeMillis() - currTime) + " [ Milli Seconds ]");
        return true;
    }

    /**
     * Checks if is server reachable.
     *
     * @return true, if is server reachable
     */
    public boolean isServerReachable() {
        if (isClustered)
            return redissonClient.getClusterNodesGroup().pingAll();
        else
            return redissonClient.getNodesGroup().pingAll();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.myntra.util.idempotent.lock.manager.BulkProcessLockManager#
     * cleanLockManager()
     */
    @Override
	public void cleanLockManager() throws Exception {
		LOGGER.info("Releasing connection with REDIS.");
		if (redissonClient != null && !redissonClient.isShutdown()) {
			redissonClient.shutdown();
		}
		LOGGER.info("REDIS connection released.");
	}

    /*
     * (non-Javadoc)
     *
     * @see com.myntra.util.idempotent.lock.manager.BulkProcessLockManager#
     * getLockInstance(java.lang.String)
     */
    public Lock getLockInstance(final String key) {
        RLock[] locks = new RLock[nodeCount + 1];
        for (int i = 0; i < nodeCount + 1; i++) {
            locks[i] = redissonClient.getFairLock(key + "#" + i);
        }
        return new RedissonRedLock(locks);
    }

    /**
     * The Enum REDISCLUSTERTYPE.
     */
    private enum REDISCLUSTERTYPE {

        /**
         * The non clustered.
         */
        NON_CLUSTERED,
        /**
         * The clustered.
         */
        CLUSTERED
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("REDISBulkProcessLockManager [redisHosts=");
		builder.append(redisHosts);
		builder.append(", isClustered=");
		builder.append(isClustered);
		builder.append(", redissonClient=");
		builder.append(redissonClient);
		builder.append(", isREDISConnected=");
		builder.append(isREDISConnected);
		builder.append(", nodeCount=");
		builder.append(nodeCount);
		builder.append("]");
		return builder.toString();
	}

}
