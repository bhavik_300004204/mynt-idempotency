package com.myntra.util.idempotent.lock.manager;

import java.util.concurrent.locks.Lock;

/**
 * The Interface BulkProcessLockManager.
 *
 * @author Bhavikkumar Ambani
 */
public interface BulkProcessLockManager {

    /**
     * Gets the lock instance which must implement
     * <code>java.util.concurrent.locks.Lock</code> interface.
     *
     * @param key the key
     * @return the lock instance
     * @throws Exception the exception
     */
    Lock getLockInstance(String key) throws Exception;

    /**
     * Locks the key.
     *
     * @param key the key
     * @return true, if successful
     * @throws Exception the exception
     */
    boolean lock(String key) throws Exception;

    /**
     * Unlocks the key.
     *∑
     * @param key the key
     * @return true, if successful
     * @throws Exception the exception
     */
    boolean unlock(String key) throws Exception;

    /**
     * Clean lock manager, which will shutdown the instance and release the connection for the .
     *
     * @throws Exception the exception
     */
    void cleanLockManager() throws Exception;
}
