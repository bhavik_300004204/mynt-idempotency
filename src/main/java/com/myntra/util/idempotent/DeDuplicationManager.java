package com.myntra.util.idempotent;

import java.util.concurrent.locks.Lock;

import javax.servlet.http.HttpServletResponse;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.myntra.util.idempotent.exception.IdempotencyException;
import com.myntra.util.idempotent.lock.manager.BulkProcessLockManager;

/**
 * The Class DeDuplicationManager.
 *
 * @author Bhavikkumar Ambani
 */
@Aspect
@Component
@EnableAspectJAutoProxy
public class DeDuplicationManager {

	private static final Logger LOGGER = LoggerFactory.getLogger(IdempotencyKeyParser.class);

    /**
     * The key parser.
     */
    private final IdempotencyKeyParser keyParser;

    /**
     * The lock manager.
     */
    private final BulkProcessLockManager lockManager;

    /**
     * Instantiates a new idempotency manager.
     *
     * @param lockManager the lock manager
     */
	public DeDuplicationManager(final BulkProcessLockManager lockManager) {
		this(lockManager, null);
		LOGGER.debug("Creating DeDuplication Manager instance with # " + lockManager);
	}

    /**
     * Instantiates a new idempotency manager.
     *
     * @param lockManager the lock manager
     */
    public DeDuplicationManager(final BulkProcessLockManager lockManager, final String keyPrefix) {
        this.lockManager = lockManager;
        keyParser = new IdempotencyKeyParser(keyPrefix);
        LOGGER.info("DeDuplicationManager Initialized.");
    }

    /**
     * Handle exception in case of Idempotency.
     *
     * @param response the response
     */
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Duplicate request found.", code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {IdempotencyException.class})
	public void handleException(HttpServletResponse response) {
		LOGGER.info("Exception occurred. ### " + response);
	}

    /**
     * Locks the idempotent request.
     *
     * @param joinPoint  the join point, before which this lock will happen
     * @param annotation the annotation
     *
     * @throws Throwable the throwable
     */
    @Before("@annotation(annotation)")
    public void lockRequest(JoinPoint joinPoint, DeDup annotation) throws Throwable {
        String idempotentKey = keyParser.getIdempotencyKey(joinPoint, annotation);
        LOGGER.info("Idempotent request, Creating lock on Key # [ " + idempotentKey + " ]");
        boolean LockAcquired = false;
        try {
            Lock lockInstance = lockManager.getLockInstance(idempotentKey);
            if (lockInstance != null) {
                LockAcquired = lockInstance.tryLock();
            }
        } catch (Exception e) {
			LOGGER.error("Exception while acquiring lock for the Idempotent Request.", e);
        }
		if (!LockAcquired)
            throw new IdempotencyException("Lock already acquired on Key # [ " + idempotentKey + " ]");
    }

    /**
     * Unlock request.
     *
     * @param joinPoint  the join point
     * @param annotation the annotation
     *
     * @throws Throwable the throwable
     */
    @After("@annotation(annotation)")
    public void unlockRequest(JoinPoint joinPoint, DeDup annotation) throws Throwable {
        String idempotentKey = keyParser.getIdempotencyKey(joinPoint, annotation);
        try {
            Lock lockInstance = lockManager.getLockInstance(idempotentKey);
            if (lockInstance != null) {
                lockInstance.unlock();
                LOGGER.info("Idempotent request, Releasing lock on Key ### [ " + idempotentKey + " ]");
            }
        } catch (Exception e) {
			LOGGER.error("Exception while releasing lock for the Idempotent Request.", e);
        }
    }

}