package com.myntra.util.idempotent.exception;

/**
 * The Class IdempotencyException.
 *
 * @author Bhavikkumar Ambani
 */
public class IdempotencyException extends Exception {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = -175364510909544426L;

    /**
     * Instantiates a new idempotency exception.
     */
    public IdempotencyException() {
        super();
    }

    /**
     * Instantiates a new idempotency exception.
     *
     * @param message            the message
     * @param cause              the cause
     * @param enableSuppression  the enable suppression
     * @param writableStackTrace the writable stack trace
     */
    public IdempotencyException(String message, Throwable cause, boolean enableSuppression,
                                boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    /**
     * Instantiates a new idempotency exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public IdempotencyException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new idempotency exception.
     *
     * @param message the message
     */
    public IdempotencyException(String message) {
        super(message);
    }

    /**
     * Instantiates a new idempotency exception.
     *
     * @param cause the cause
     */
    public IdempotencyException(Throwable cause) {
        super(cause);
    }

}
