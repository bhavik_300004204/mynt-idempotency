package com.myntra.util.idempotent.types;

/**
 * The Enum DeDupParamType which will categorize the type of the key to be identified as idempotent key.
 *
 * @author Bhavikkumar Ambani
 */
public enum DeDupParamType {

    /**
     * The request parameter.
     */
    REQUEST_PARAM,
    /**
     * The request parameter property of the request object parameter.
     */
    REQUEST_PARAM_PROPERTY,
    /**
     * The multiple key parameter property should have format
     * {"RequestParameter1":["Field1","Field2","Field2"],"RequestParameter1":["Par2Field1","Par2Field2","Par2Field2"]}
     */
    MULTI_KEY_PARAM
}
