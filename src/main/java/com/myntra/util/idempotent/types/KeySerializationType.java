package com.myntra.util.idempotent.types;

/**
 * The Enum KeySerializationType.
 * 
 * @author Bhavik Ambani
 */
public enum KeySerializationType {

	/** The to string. */
	TO_STRING,
	/** The hashcode. */
	HASHCODE,
	/** The md5. */
	MD5
}
