package com.myntra.util.idempotent;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import com.myntra.util.idempotent.types.DeDupParamType;
import com.myntra.util.idempotent.types.KeySerializationType;

/**
 * The Interface Idempotent.
 *
 * @author Bhavikkumar Ambani
 */
@Target(value = { java.lang.annotation.ElementType.METHOD, java.lang.annotation.ElementType.TYPE })
@Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
public @interface DeDup {

	/**
	 * Class property name.
	 *
	 * @return the string
	 */
	String classPropertyName() default "";

	/**
	 * Key object.
	 *
	 * @return the string
	 */
	String keyObject();

	/**
	 * Key serialization type.
	 *
	 * @return the key serialization type
	 */
	KeySerializationType keySerializationType() default KeySerializationType.TO_STRING;

	/**
	 * Name.
	 *
	 * @return the string
	 */
	String name() default "";

	/**
	 * Defines the Idempotent key type, which should be of any of the
	 * {@link DeDupParamType} enum.
	 *
	 * @return the idempotency param type
	 */
	DeDupParamType paramType() default DeDupParamType.REQUEST_PARAM;

	/**
	 * Type.
	 *
	 * @return the class
	 */
	Class<?> type() default Object.class;
}