## idempotent ##
At given point of time only single request should be in process for the request with the same parameters
 
 
## Implementation document ##
To maintain idempotent request, we will store the key in REDIS for the given request with parameter
REDIS RedLock algorithm is used in this case : https://redis.io/topics/distlock

 
## Locking implementation ##
We have to provide key on which we want to acquire lock.
N + 1 keys will be generated and lock will be acquired in each of the key in REDIS [ Here N is the number of write nodes in the REDIS. ]
Once, the lock is acquired, then only actual business logic will be called.
Once, the method invocation is completed, the lock will be released and another request can be processed with the same parameter.
 
 
## Making request idempotent. ##
Idempotency key can be set with the following three type of key
Key as request parameter [ Default ]
Key as request object and any property of that object
Key as JSON which will contain multiple request parameter and multiple properties of that request parameter.
 
 
1) Idempotent key as request parameter

*  It can be set as following way with  paramType = IdempotencyParamType.REQUEST_PARAM) , where we can use directly as request parameter as a key for REDIS.

* In below example, key would be the value of userId field i.e. 99740

```
#!java

	@GET
	@Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
	@Idempotent(keyObject = "id", paramType = IdempotencyParamType.REQUEST_PARAM)
    public Response getDiscount(@PathParam("id") Long id) {
            .
            .
            .
            .
            .
            .
      }
```

2) Idempotent key as some property of the request parameter

* Here note that there is two different property is set for the annotation, keyObject and classPropertyName, here keyObject is the request parameter name and classPropertyName is the property of that request object.

* In below example key would be the value property of the id entity. E.g. 123#

```
#!java

	@GET
	@Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
	@Idempotent(keyObject = "id", classPropertyName = "value", paramType = IdempotencyParamType.REQUEST_PARAM_PROPERTY)
    public Response getDiscount(@PathParam("id") Long id) {
    	Discount discount = manager.getDiscountById(id);

            .
            .
            .
            .
            .
            .
      }
```

3) Idempotent key as multiple properties

* When we want to set key with multiple properties of the request, then it can be configured with a predefined format of JSON as keyObject parameter

* In below example key would be multiple properties of the object separated by '#' i.e. ABC#true#10#1000

```
#!java

	@Idempotent(paramType = IdempotencyParamType.JSON, keyObject = "\"{\"styleCapMap\":[\"strVal\",\"boolVal\"],\"sampleEntity\":[\"code\"]}\"")
	public SampleEntity insertJSON(SampleDataEntry styleCapMap, SampleEntity sampleEntity) {
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Hello called." + styleCapMap);
		return null;
	}
```